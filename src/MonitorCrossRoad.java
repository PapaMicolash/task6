import java.awt.*;
import java.util.ArrayList;

public class MonitorCrossRoad {

    public static final int POSITION_BY_X_START = (int)Settings.POINT_CROSS_ROAD.getX() - Settings.WIDTH_SQUARE;
    public static final int POSITION_BY_X_END = (int)Settings.POINT_CROSS_ROAD.getX() + (int)Settings.DIMENSION_CROSS_ROAD.getWidth();
    public static final int POSITION_BY_Y_START = (int)Settings.POINT_CROSS_ROAD.getY() - Settings.HEIGHT_SQUARE;
    public static final int POSITION_BY_Y_END = (int)Settings.POINT_CROSS_ROAD.getY() + (int)Settings.DIMENSION_CROSS_ROAD.getHeight();

    private static MonitorCrossRoad ourInstance = new MonitorCrossRoad();

    public static MonitorCrossRoad getInstance() {
        return ourInstance;
    }

    private MonitorCrossRoad() {
    }

    Thread thread = null;
    boolean checkCrossroad = false;
    Color curColor = null;
    ArrayList<Thread> colorThreads = new ArrayList<>();

    public synchronized void controller(int x, int y, boolean horVert){
        if (!checkCrossroad) {
            if (getPosition(x, y, horVert) == 1 || getPosition(x, y, horVert) == 2){
                thread = Thread.currentThread();
                checkCrossroad = true;
            }
        } else {
            if (Thread.currentThread() == thread) {
                if (getPosition(x, y, horVert) == 3 || getPosition(x, y, horVert) == 4) {
                    thread = null;
                    checkCrossroad = false;
                    notify();
                }
            } else {
                if (getPosition(x, y, horVert) == 1 || getPosition(x, y, horVert) == 2) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        }
    }


    public int getPosition(int x, int y, boolean horVert) {
        if (horVert) {
            if (x >= POSITION_BY_X_START && x<= POSITION_BY_X_END) {
                return 1;
            }

            if (x < POSITION_BY_X_START || x > POSITION_BY_X_START) {
                return 3;
            } else {
                return 5;
            }
        } else {
            if (y >= POSITION_BY_Y_START  && y <= POSITION_BY_Y_END) {
                return 2;
            }

            if (y < POSITION_BY_Y_START || y > POSITION_BY_Y_END) {
                return 4;
            } else {
                return 6;
            }
        }
    }

    public synchronized boolean controllerByColor(int x, int y, boolean horVert, Color color) {
        if (!checkCrossroad) {
            if (getPosition(x, y, horVert) == 1 || getPosition(x, y, horVert) == 2){
                checkCrossroad = true;
                curColor = color;
                thread = Thread.currentThread();
                colorThreads.add(Thread.currentThread());
            }
        } else {
            for (int i = 0; i < colorThreads.size(); i++) {
                if (Thread.currentThread() == colorThreads.get(i) &&
                        getPosition(x, y, horVert) == 3 || getPosition(x, y, horVert) == 4) {
                    colorThreads.remove(Thread.currentThread());
                }
            }

            if ((getPosition(x, y, horVert) == 1 || getPosition(x, y, horVert) == 2) && color.equals(curColor) ) {
                colorThreads.add(Thread.currentThread());
            } else if ((getPosition(x, y, horVert) == 1 || getPosition(x, y, horVert) == 2) && !color.equals(curColor)) {

                try {
                    wait();
                    return false;

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        if (colorThreads.size() == 0) {
            curColor = null;
            checkCrossroad = false;
            notifyAll();

        }

        return true;
    }


}
