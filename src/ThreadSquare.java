import javax.swing.*;
import java.awt.*;
import java.util.Random;

/**
 * Created by vcnuv on 07.06.2018.
 */
public class ThreadSquare implements Runnable{
    public final boolean horVert;
    public int x, y;
    private int direction = 1;
    JButton button;
    private final Container contentPane;
    MonitorCrossRoad mcr = MonitorCrossRoad.getInstance();
    Color curColor;
    public boolean numTask = Settings.numberTask;
    public boolean checkWait = true;



    public ThreadSquare(boolean horVert){
        this.horVert = horVert;
        if(horVert){
            x = 0;
            y = Settings.POINT_CROSS_ROAD.y + (int)(Math.random() * (Settings.DIMENSION_CROSS_ROAD.height - Settings.HEIGHT_SQUARE));
        } else {
            x = Settings.POINT_CROSS_ROAD.x + (int)(Math.random() * (Settings.DIMENSION_CROSS_ROAD.width - Settings.WIDTH_SQUARE));
            y = 0;
        }
        this.contentPane = Settings.contentPane;
        initButton();
    }

    @Override
    public void run(){
        while(true){
            if (numTask) {
                mcr.controller(x, y, horVert);
            } else {
                checkWait = mcr.controllerByColor(x, y, horVert, curColor);
        }

            if (!checkWait) {
                checkWait = true;
            } else {
                move();
                getNextPoint();

            }
        }
    }

    private void getNextPoint(){
        if(horVert){
            x += Settings.STEP_SQUARE * direction;
            if(x >= Settings.FRAME_WIDTH - Settings.WIDTH_SQUARE){
                direction = -1;
            }
            if(x<=0){
                direction = 1;
            }
        } else {
            y += Settings.STEP_SQUARE * direction;
            if(y>=Settings.FRAME_HEIGHT - Settings.HEIGHT_SQUARE - 24){
                direction = -1;
            }
            if(y<=0){
                direction = 1;
            }
        }
    }

    private void move(){
        button.setBounds(x, y, Settings.WIDTH_SQUARE, Settings.HEIGHT_SQUARE);
        try {
            Thread.sleep(Settings.DELAY_SQUARE);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void initButton(){
        Random random = new Random();
        Color[] color = {new Color(0,0,0), new Color(255,0,0), new Color(0,255,0),new Color(255,165,122),
                new Color(255,215,0),new Color(0,0,255),new Color(255,0,255),new Color(190,190,190)};
        button = new JButton("" + (++Settings.counter));
        button.setLocation(x,y);
        button.setSize(Settings.WIDTH_SQUARE, Settings.HEIGHT_SQUARE);
        button.setMargin(new Insets(0,0,0,0));
        button.setForeground(Settings.COLOR_TEXT_SQUARE);
        curColor = color[random.nextInt(color.length)];
        button.setBackground(curColor);
        contentPane.add(button);
    }
}
